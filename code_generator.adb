with ASTs; use ASTs;

package body Code_Generator is
    function Assemble(
        AST_List : in AST_Vector.Vector;
        Labels : in out Label_Map.Map;
        Fixes : out Fix_Map.Map)
    return Byte_Vector.Vector is
        Bytes : Byte_Vector.Vector;
        PC : UInt32 := 0;
        Opcode : UInt32;
        Supervisor : Boolean := FALSE;

        procedure Each_AST(Position : AST_Vector.Cursor) is
            Cur_AST : AST := AST_Vector.Element(Position);
        begin
            case Cur_AST.Tag is
            when Label_AST =>
                Label_Map.Insert(Labels, Cur_AST.Str, PC);
            when Bias_AST =>
                PC := Cur_AST.Imm;
            when Realm_AST =>
                Supervisor := Cur_AST.Mode = 1;
            when Fill_AST =>
                PC := PC + Cur_AST.Imm;
                for I in 1..Cur_AST.Imm loop
                    Byte_Vector.Append(Bytes, UInt8(Cur_AST.R0));
                end loop;
            when Instruction_AST =>
                declare
                    Instr : String := To_String(Cur_AST.Str);
                    Mnemonic : Unbounded_String;
                    Condition : Unbounded_String;
                begin
                    for I in Instr'Range loop
                        if Instr(I) = '.' then
                            Append(Condition, Instr(I..Instr'Last));
                            exit;
                        end if;
                        Append(Mnemonic, Instr(I));
                    end loop;
                    Opcode := Get_Opcode(To_String(Mnemonic));
                    Opcode := Opcode or
                              Get_Condition(To_String(Condition)) * 2**28;
                end;
                Opcode := Opcode or
                          (Fix_Register(Cur_AST.R0, Supervisor) * 2**12);
                Opcode := Opcode or
                          (Fix_Register(Cur_AST.R1, Supervisor) * 2**6);
                Opcode := Opcode or
                          Fix_Register(Cur_AST.R2, Supervisor);
                if Cur_AST.Mode = 1 then
                    Opcode := Opcode or 2**20;
                end if;
                Byte_Vector.Append(Bytes, UInt8((Opcode / 2**24) and 255));
                Byte_Vector.Append(Bytes, UInt8((Opcode / 2**16) and 255));
                Byte_Vector.Append(Bytes, UInt8((Opcode / 2**8) and 255));
                Byte_Vector.Append(Bytes, UInt8(Opcode and 255));
                PC := PC + 4;
                if Cur_AST.Mode = 1 then
                    PC := PC + 4;
                    Byte_Vector.Append(
                        Bytes,
                        UInt8((Cur_AST.Imm / 2**24) and 255));
                    Byte_Vector.Append(
                        Bytes,
                        UInt8((Cur_AST.Imm / 2**16) and 255));
                    Byte_Vector.Append(
                        Bytes,
                        UInt8((Cur_AST.Imm / 2**8) and 255));
                    Byte_Vector.Append(
                        Bytes,
                        UInt8(Cur_AST.Imm and 255));
                    if Length(Cur_AST.ImmS) > 0 then
                        Fix_Map.Insert(
                            Fixes,
                            UInt32(Byte_Vector.To_Index(
                                Byte_Vector.Last(Bytes)) - 3),
                            Cur_AST.ImmS);
                    end if;
                end if;
            end case;
        end Each_AST;
    begin
        AST_Vector.Iterate(AST_List, Each_AST'Access);
        return Bytes;
    end Assemble;

    function Get_Opcode(Mnemonic : in String)
    return UInt32 is
    begin
        if Mnemonic = "add" then
            return 16#00000000#;
        elsif Mnemonic = "addf" then
            return 16#00200000#;
        elsif Mnemonic = "sub" then
            return 16#00400000#;
        elsif Mnemonic = "subf" then
            return 16#00600000#;
        elsif Mnemonic = "mul" then
            return 16#00800000#;
        elsif Mnemonic = "mulf" then
            return 16#00A00000#;
        elsif Mnemonic = "div" then
            return 16#00C00000#;
        elsif Mnemonic = "divf" then
            return 16#00E00000#;
        elsif Mnemonic = "shr" then
            return 16#01000000#;
        elsif Mnemonic = "shl" then
            return 16#01400000#;
        elsif Mnemonic = "ror" then
            return 16#01800000#;
        elsif Mnemonic = "rol" then
            return 16#01C00000#;
        elsif Mnemonic = "xor" then
            return 16#02000000#;
        elsif Mnemonic = "and" then
            return 16#02400000#;
        elsif Mnemonic = "or" then
            return 16#02800000#;
        elsif Mnemonic = "not" then
            return 16#02C00000#;
        elsif Mnemonic = "chk" then
            return 16#03400000#;
        elsif Mnemonic = "ld" then
            return 16#04080000#;
        elsif Mnemonic = "lds" then
            return 16#04040000#;
        elsif Mnemonic = "ldb" then
            return 16#04000000#;
        elsif Mnemonic = "st" then
            return 16#04480000#;
        elsif Mnemonic = "sts" then
            return 16#04440000#;
        elsif Mnemonic = "stb" then
            return 16#04400000#;
        elsif Mnemonic = "ldr" then
            return 16#04280000#;
        elsif Mnemonic = "ldrs" then
            return 16#04240000#;
        elsif Mnemonic = "ldrb" then
            return 16#04200000#;
        elsif Mnemonic = "str" then
            return 16#04680000#;
        elsif Mnemonic = "strs" then
            return 16#04640000#;
        elsif Mnemonic = "strb" then
            return 16#04600000#;
        elsif Mnemonic = "pop" then
            return 16#04880000#;
        elsif Mnemonic = "pops" then
            return 16#04840000#;
        elsif Mnemonic = "popb" then
            return 16#04800000#;
        elsif Mnemonic = "push" then
            return 16#04C80000#;
        elsif Mnemonic = "pushs" then
            return 16#04C40000#;
        elsif Mnemonic = "pushb" then
            return 16#04C00000#;
        elsif Mnemonic = "sid" then
            return 16#06400000#;
        elsif Mnemonic = "sie" then
            return 16#06600000#;
        elsif Mnemonic = "lmsr" then
            return 16#06800000#;
        elsif Mnemonic = "smsr" then
            return 16#06A00000#;
        elsif Mnemonic = "ume" then
            return 16#07000000#;
        elsif Mnemonic = "bl" then
            return 16#03000000#;
        elsif Mnemonic = "swi" then
            return 16#03400000#;
        elsif Mnemonic = "hlt" then
            return 16#06000000#;
        elsif Mnemonic = "rfi" then
            return 16#06C00000#;
        elsif Mnemonic = "call" then
            return 16#07400000#;
        elsif Mnemonic = "ret" then
            return 16#07800000#;
        elsif Mnemonic = "popm" then
            return 16#08000000#;
        elsif Mnemonic = "pushm" then
            return 16#08400000#;
        elsif Mnemonic = "mod" then
            return 16#09000000#;
        elsif Mnemonic = "modf" then
            return 16#09200000#;
        elsif Mnemonic = "fti" then
            return 16#09400000#;
        elsif Mnemonic = "itf" then
            return 16#09600000#;
        elsif Mnemonic = "mulu" then
            return 16#0A000000#;
        elsif Mnemonic = "divu" then
            return 16#0A400000#;
        elsif Mnemonic = "hyp" then
            return 16#0F000000#;
        elsif Mnemonic = "brk" then
            return 16#0F400000#;
        else
            raise Code_Generator_Error with "invalid instruction " & Mnemonic;
        end if;
    end Get_Opcode;

    function Get_Condition(Condition : in String)
    return UInt32 is
    begin
        if Condition = ".al" or Condition = "" then
            return 0;
        elsif Condition = ".eq" then
            return 1;
        elsif Condition = ".ne" then
            return 2;
        elsif Condition = ".cs" then
            return 3;
        elsif Condition = ".cc" then
            return 4;
        elsif Condition = ".hi" then
            return 5;
        elsif Condition = ".ls" then
            return 6;
        elsif Condition = ".ge" then
            return 7;
        elsif Condition = ".lt" then
            return 8;
        elsif Condition = ".gt" then
            return 9;
        elsif Condition = ".le" then
            return 10;
        elsif Condition = ".mi" then
            return 11;
        elsif Condition = ".pl" then
            return 12;
        elsif Condition = ".vs" then
            return 13;
        elsif Condition = ".vc" then
            return 14;
        elsif Condition = ".el" then
            return 15;
        else
            raise Code_Generator_Error with "invalid condition " & Condition;
        end if;
    end Get_Condition;

    function Fix_Register(Reg : in Integer; Supervisor : in Boolean)
    return UInt32 is
        R : Integer := Reg;
    begin
        if R >= 64 then
            R := R - 64;
            if Supervisor then
                R := R + 32;
            end if;
        end if;
        return UInt32(R);
    end Fix_Register;

    procedure Fix_Binary(
        Bytes : in out Byte_Vector.Vector;
        Labels : in Label_Map.Map;
        Fixes : in Fix_Map.Map) is
        Position : Fix_Map.Cursor := Fix_Map.First(Fixes);
        Key : Natural;
        Value : UInt32;
        Acc : UInt32;
    begin
        while Fix_Map.Has_Element(Position) loop
            Key := Natural(Fix_Map.Key(Position));
            Value := Label_Map.Element(Labels, Fix_Map.Element(Position));
            Position := Fix_Map.Next(Position);
            Acc := (UInt32(Byte_Vector.Element(Bytes, Key)) * 2**24) +
                   (UInt32(Byte_Vector.Element(Bytes, Key + 1)) * 2**16) +
                   (UInt32(Byte_Vector.Element(Bytes, Key + 2)) * 2**8) +
                   UInt32(Byte_Vector.Element(Bytes, Key + 3));
            Acc := Acc + Value;
            Byte_Vector.Replace_Element(
                Bytes, Key, UInt8((Acc / 2**24) and 255));
            Byte_Vector.Replace_Element(
                Bytes, Key + 1, UInt8((Acc / 2**16) and 255));
            Byte_Vector.Replace_Element(
                Bytes, Key + 2, UInt8((Acc / 2**8) and 255));
            Byte_Vector.Replace_Element(
                Bytes, Key + 3, UInt8(Acc and 255));
        end loop;
    end Fix_Binary;
end Code_Generator;
