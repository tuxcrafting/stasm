with Ada.Containers.Vectors;

with Tokens; use Tokens;

package Lexer is
    package Token_Vector is new Ada.Containers.Vectors(Natural, Token);

    Lexer_Error : exception;

    function Lex(Code : in String; Index : in out Natural)
    return Token;
    function Lex_All(Code : in String)
    return Token_Vector.Vector;
    procedure Skip_Whitespace(Code : in String; Index : in out Natural);
    function Get(Code : in String; Index : in Natural)
    return Character;
    function Is_Identifier(Char : in Character)
    return Boolean;
end Lexer;
