with Ada.Containers.Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Parser; use Parser;
with Util; use Util;

package Code_Generator is
    package Label_Map is new Ada.Containers.Ordered_Maps(
        Unbounded_String, UInt32);
    package Fix_Map is new Ada.Containers.Ordered_Maps(
        UInt32, Unbounded_String);
    package Byte_Vector is new Ada.Containers.Vectors(Natural, UInt8);

    Code_Generator_Error : exception;

    function Assemble(
        AST_List : in AST_Vector.Vector;
        Labels : in out Label_Map.Map;
        Fixes : out Fix_Map.Map)
    return Byte_Vector.Vector;

    function Get_Opcode(Mnemonic : in String)
    return UInt32;

    function Get_Condition(Condition : in String)
    return UInt32;

    function Fix_Register(Reg : in Integer; Supervisor : in Boolean)
    return UInt32;

    procedure Fix_Binary(
        Bytes : in out Byte_Vector.Vector;
        Labels : in Label_Map.Map;
        Fixes : in Fix_Map.Map);
end Code_Generator;
