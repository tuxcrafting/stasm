package Util is
    type UInt8 is mod 2**8;
    type UInt32 is mod 2**32;
end Util;
