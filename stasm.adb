with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Command_Line;
with Ada.Containers.Vectors;
with Ada.Direct_IO;
with Ada.Directories;
with Ada.IO_Exceptions;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO;

with Code_Generator; use Code_Generator;
with Lexer; use Lexer;
with Parser; use Parser;
with Tokens; use Tokens;
with Util; use Util;

procedure STASM is
    package CL renames Ada.Command_Line;
    package T_IO renames Ada.Text_IO;

    package US_Vector is new Ada.Containers.Vectors(Natural, Unbounded_String);

    type Arg_Name_Type is (AT_None, AT_O, AT_I, AT_T);
    type Output_Format_Type is (Bin, RCF);

    package Output_Format_IO is new T_IO.Enumeration_IO(Output_Format_Type);

    Arg_Name : Arg_Name_Type := AT_None;

    Output_File : Unbounded_String;
    Output_File_Defined : Boolean := FALSE;
    Input_Files : US_Vector.Vector;
    Position : US_Vector.Cursor;
    Output_Format : Output_Format_Type := Bin;
    AST_List : AST_Vector.Vector;
    Labels : Label_Map.Map;
    Fixes : Fix_Map.Map;
    Bytes : Byte_Vector.Vector;
begin
    for I in 1..CL.Argument_Count loop
        declare
            Arg : String := CL.Argument(I);
        begin
            if Arg'Length > 0 and
               Arg(Arg'First) = '-' and
               Arg_Name = AT_None then
                if Arg = "-h" or
                   Arg = "--help" then
                    goto Print_Usage;
                elsif Arg = "-v" or
                      Arg = "--version" then
                    T_IO.Put_Line("STASM v0.1");
                    T_IO.Put_Line("Copyright (c) 2018 tuxcrafting");
                    T_IO.Put("Supported output formats: ");
                    for Format in Output_Format_Type loop
                        Output_Format_IO.Put(
                            Format,
                            Output_Format_IO.Default_Width,
                            T_IO.Lower_Case);
                        T_IO.Put(" ");
                    end loop;
                    T_IO.New_Line;
                    return;
                elsif Arg = "-o" or
                      Arg = "--output" then
                    Arg_Name := AT_O;
                elsif Arg = "-i" or
                      Arg = "--input" then
                    Arg_Name := AT_I;
                elsif Arg = "-t" or
                      Arg = "--type" then
                    Arg_Name := AT_T;
                elsif Arg = "-j" or
                      Arg = "--object" then
                    Output_Format := RCF;
                else
                    T_IO.Put_Line("Invalid argument: " & Arg);
                    goto Print_Usage;
                end if;
            else
                case Arg_Name is
                when AT_O =>
                    Arg_Name := AT_None;
                    Output_File_Defined := TRUE;
                    Output_File := To_Unbounded_String(Arg);
                when AT_T =>
                    Arg_Name := AT_None;
                    begin
                        Output_Format := Output_Format_Type'Value(Arg);
                    exception
                    when Constraint_Error =>
                        T_IO.Put("Warning: Invalid output format ");
                        T_IO.Put_Line(Arg);
                    end;
                when others =>
                    Arg_Name := AT_None;
                    US_Vector.Append(Input_Files, To_Unbounded_String(Arg));
                end case;
            end if;
        end;
    end loop;
    if Arg_Name /= AT_None then
        T_IO.Put_Line("Expected argument value");
        goto Print_Usage;
    end if;
    if Natural(US_Vector.Length(Input_Files)) = 0 then
        T_IO.Put_Line("Error: No input files");
        return;
    end if;
    if Output_File_Defined = FALSE then
        Output_File := Null_Unbounded_String;
        declare
            Input_File : String :=
                To_String(US_Vector.Element(US_Vector.First(Input_Files)));
        begin
            for I in Input_File'Range loop
                exit when Input_File(I) = '.';
                Append(Output_File, Input_File(I));
            end loop;
            Append(Output_File, ".bin");
        end;
    end if;

    Position := US_Vector.First(Input_Files);
    while US_Vector.Has_Element(Position) loop
        declare
            File_Name : String := To_String(US_Vector.Element(Position));
            File_Size : Natural;
        begin
            Position := US_Vector.Next(Position);
            File_Size := Natural(Ada.Directories.Size(File_Name));
            declare
                subtype File_String is String(1..File_Size);
                package File_String_IO is new Ada.Direct_IO(File_String);

                File : File_String_IO.File_Type;
                Contents : File_String;

                Tokens : Token_Vector.Vector;
            begin
                File_String_IO.Open(
                    File,
                    Mode => File_String_IO.In_File,
                    Name => File_Name);
                File_String_IO.Read(File, Item => Contents);
                File_String_IO.Close(File);
                Tokens := Lex_All(Contents);
                AST_Vector.Append(AST_List, Parse_All(Tokens));
            end;
        exception
        when Ada.IO_Exceptions.Name_Error =>
            T_IO.Put("Error: Nonexistent file ");
            T_IO.Put_Line(File_Name);
            return;
        end;
    end loop;

    Bytes := Assemble(AST_List, Labels, Fixes);
    Fix_Binary(Bytes, Labels, Fixes);

    if Output_Format /= Bin then
        T_IO.Put_Line("RCF is not yet supported");
        return;
    end if;
    declare
        type Buffer_Type is array (1..Natural(Byte_Vector.Length(Bytes))) of UInt8;
        Buffer : Buffer_Type;
        package File_String_IO is new Ada.Direct_IO(Buffer_Type);
        Position : Byte_Vector.Cursor := Byte_Vector.First(Bytes);
        Index : Natural := Buffer'First;
        File : File_String_IO.File_Type;
    begin
        while Byte_Vector.Has_Element(Position) loop
            Buffer(Index) := Byte_Vector.Element(Position);
            Index := Index + 1;
            Position := Byte_Vector.Next(Position);
        end loop;
        File_String_IO.Create(
            File,
            Mode => File_String_IO.Out_File,
            Name => To_String(Output_File));
        if Natural(Byte_Vector.Length(Bytes)) > 0 then
            File_String_IO.Write(File, Item => Buffer);
        end if;
        File_String_IO.Close(File);
    end;
    return;
<<Print_Usage>>
    T_IO.Put_Line("Usage: " & CL.Command_Name & " [options] file...");
    T_IO.Put_Line(HT & "-h, --help"
                & HT & "Print this help message");
    T_IO.Put_Line(HT & "-v, --version"
                & HT & "Print a version message");
    T_IO.Put_Line(HT & "-o file, --output file"
                & HT & "Output file (defaults to <first file>.bin)");
    T_IO.Put_Line(HT & "-i file, --input file"
                & HT & "Explicitely specify an input file");
    T_IO.Put_Line(HT & "-t type, --type type"
                & HT & "Specify the output file format (defaults to bin)");
    T_IO.Put_Line(HT & "-j, --object"
                & HT & "Equivalent to -t rcf");
end STASM;
