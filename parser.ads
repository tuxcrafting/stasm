with Ada.Containers.Vectors;

with ASTs; use ASTs;
with Lexer; use Lexer;
with Tokens; use Tokens;

package Parser is
    package AST_Vector is new Ada.Containers.Vectors(Natural, AST);

    Parser_Error : exception;

    function Parse_Line(Position : in out Token_Vector.Cursor)
    return AST_Vector.Vector;

    function Parse_All(Tokens : in Token_Vector.Vector)
    return AST_Vector.Vector;

    function Element_Or_EOF(Position : in Token_Vector.Cursor)
    return Token;
end Parser;
