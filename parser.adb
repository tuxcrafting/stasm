with Ada.Characters.Handling; use Ada.Characters.Handling;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Lexer; use Lexer;
with Tokens; use Tokens;
with Util; use Util;

package body Parser is
    function Parse_Line(Position : in out Token_Vector.Cursor)
    return AST_Vector.Vector is
        AST_Vec : AST_Vector.Vector;
        Tok : Token;
        AST_Elm : AST;
        Count : Natural := 0;
        Int : UInt32;
    begin
        AST_Elm.Str := Null_Unbounded_String;
        AST_Elm.R0 := 0;
        AST_Elm.R1 := 0;
        AST_Elm.R2 := 0;
        AST_Elm.Imm := 0;
        AST_Elm.ImmS := Null_Unbounded_String;
        AST_Elm.Mode := 0;
        Tok := Element_Or_EOF(Position);
        while Tok.Tag = Colon_Token loop
            Position := Token_Vector.Next(Position);
            Tok := Element_Or_EOF(Position);
            if Tok.Tag /= Identifier_Token then
                raise Parser_Error with "expected identifier for label";
            end if;
            AST_Elm.Tag := Label_AST;
            AST_Elm.Str := Tok.Str;
            AST_Vec.Append(AST_Elm);
            Position := Token_Vector.Next(Position);
            Tok := Element_Or_EOF(Position);
        end loop;
        if Tok.Tag = EOF_Token or Tok.Tag = Newline_Token then
            Position := Token_Vector.Next(Position);
            return AST_Vec;
        end if;
        if Tok.Tag /= Identifier_Token then
            raise Parser_Error with "expected instruction";
        end if;
        declare
            Tok_Str : String := To_Lower(To_String(Tok.Str));
        begin
            if Tok_Str = "bias" then
                Position := Token_Vector.Next(Position);
                Tok := Element_Or_EOF(Position);
                if Tok.Tag /= Integer_Token then
                    raise Parser_Error with "expected integer";
                end if;
                AST_Elm.Tag := Bias_AST;
                AST_Elm.Imm := Tok.Int;
                Position := Token_Vector.Next(Position);
            elsif Tok_Str = "realm" then
                Position := Token_Vector.Next(Position);
                Tok := Element_Or_EOF(Position);
                if Tok.Tag /= Identifier_Token then
                    raise Parser_Error with "expected identifier";
                end if;
                declare
                    Tok_Str : String := To_Lower(To_String(Tok.Str));
                begin
                    if Tok_Str = "supervisor" then
                        AST_Elm.Mode := 1;
                    elsif Tok_Str = "user" then
                        AST_Elm.Mode := 0;
                    else
                        raise Parser_Error
                            with "expected either supervisor or user";
                    end if;
                end;
                AST_Elm.Tag := Realm_AST;
                Position := Token_Vector.Next(Position);
            elsif Tok_Str = "fill" then
                AST_Elm.Tag := Fill_AST;
                loop
                    Count := Count + 1;
                    if Count > 3 then
                        raise Parser_Error with "too much operands";
                    end if;
                    Position := Token_Vector.Next(Position);
                    Tok := Element_Or_EOF(Position);
                    case Tok.Tag is
                    when Integer_Token =>
                        Int := Tok.Int;
                        case Count is
                        when 1 =>
                            AST_Elm.Imm := Int;
                        when 2 =>
                            AST_Elm.R0 := Integer(Int);
                        when others =>
                            null;
                        end case;
                    when EOF_Token | Newline_Token =>
                        exit;
                    when others =>
                        raise Parser_Error with "unexpected " &
                                                Token_Type'Image(Tok.Tag);
                    end case;
                    Position := Token_Vector.Next(Position);
                    Tok := Element_Or_EOF(Position);
                    exit when Tok.Tag /= Comma_Token;
                end loop;
            else
                AST_Elm.Tag := Instruction_AST;
                AST_Elm.Str := To_Unbounded_String(Tok_Str);
                loop
                    Count := Count + 1;
                    if Count > 3 then
                        raise Parser_Error with "too much operands";
                    end if;
                    Position := Token_Vector.Next(Position);
                    Tok := Element_Or_EOF(Position);
                    case Tok.Tag is
                    when Register_Token | Sv_Register_Token =>
                        Int := Tok.Int;
                        if Tok.Tag = Sv_Register_Token then
                            Int := Int + 64;
                        end if;
                        case Count is
                        when 1 =>
                            AST_Elm.R0 := Integer(Int);
                        when 2 =>
                            AST_Elm.R1 := Integer(Int);
                        when 3 =>
                            AST_Elm.R2 := Integer(Int);
                        when others =>
                            null;
                        end case;
                    when EOF_Token | Newline_Token =>
                        exit;
                    when others =>
                        raise Parser_Error with "unexpected " &
                                                Token_Type'Image(Tok.Tag);
                    end case;
                    Position := Token_Vector.Next(Position);
                    Tok := Element_Or_EOF(Position);
                    exit when Tok.Tag /= Comma_Token;
                end loop;
                case Tok.Tag is
                when Colon_Token =>
                    Position := Token_Vector.Next(Position);
                    Tok := Element_Or_EOF(Position);
                    Position := Token_Vector.Next(Position);
                    AST_Elm.Mode := 1;
                    case Tok.Tag is
                    when Integer_Token =>
                        AST_Elm.Imm := Tok.Int;
                    when Identifier_Token =>
                        AST_Elm.ImmS := Tok.Str;
                    when others =>
                        raise Parser_Error with "unexpected " &
                                                Token_Type'Image(Tok.Tag);
                    end case;
                when EOF_Token | Newline_Token =>
                    null;
                when others =>
                    raise Parser_Error with "unexpected " &
                                            Token_Type'Image(Tok.Tag);
                end case;
            end if;
            AST_Vec.Append(AST_Elm);
        end;
        Tok := Element_Or_EOF(Position);
        if not (Tok.Tag = EOF_Token or Tok.Tag = Newline_Token) then
            raise Parser_Error with "expected EOF or newline";
        end if;
        Position := Token_Vector.Next(Position);
        return AST_Vec;
    end Parse_Line;

    function Parse_All(Tokens : in Token_Vector.Vector)
    return AST_Vector.Vector is
        Position : Token_Vector.Cursor := Token_Vector.First(Tokens);
        AST_Vec : AST_Vector.Vector;
    begin
        while Element_Or_EOF(Position).Tag /= EOF_Token loop
            AST_Vector.Append(AST_Vec, Parse_Line(Position));
        end loop;
        return AST_Vec;
    end;

    function Element_Or_EOF(Position : in Token_Vector.Cursor)
    return Token is
        Tok : Token;
    begin
        return Token_Vector.Element(Position);
    exception
    when Constraint_Error =>
        Tok.Tag := EOF_Token;
        Tok.Str := Null_Unbounded_String;
        Tok.Int := 0;
        return Tok;
    end Element_Or_EOF;
end Parser;
