with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Util; use Util;

package ASTs is
    type AST_Type is (
        Instruction_AST, Label_AST, Bias_AST, Realm_AST, Fill_AST);
    type AST is record
        Tag : AST_Type;
        Str : Unbounded_String;
        R0 : Integer;
        R1 : Integer;
        R2 : Integer;
        Imm : UInt32;
        ImmS : Unbounded_String;
        Mode : Integer;
    end record;
end ASTs;
