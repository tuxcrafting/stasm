with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Util; use Util;

package Tokens is
    type Token_Type is (
        Identifier_Token, Register_Token, Sv_Register_Token, Integer_Token,
        Comma_Token, Colon_Token, Newline_Token, EOF_Token);

    type Token is record
        Tag : Token_Type;
        Str : Unbounded_String;
        Int : UInt32;
    end record;
end Tokens;
