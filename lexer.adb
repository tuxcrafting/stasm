with Ada.Characters.Handling; use Ada.Characters.Handling;
with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Util; use Util;

package body Lexer is
    function Lex(Code : in String; Index : in out Natural)
    return Token is
        Tok : Token;
    begin
        Tok.Str := Null_Unbounded_String;
        Tok.Int := 0;
        Skip_Whitespace(Code, Index);
        if Get(Code, Index) = ';' then
            while Index < Code'Last and Get(Code, Index) /= LF loop
                Index := Index + 1;
            end loop;
            Skip_Whitespace(Code, Index);
        end if;
        if Index >= Code'Last then
            Tok.Tag := EOF_Token;
            return Tok;
        end if;
        if Get(Code, Index) in '0'..'9' then
            Tok.Tag := Integer_Token;
            if Get(Code, Index + 1) = 'x' then
                Index := Index + 2;
                while Get(Code, Index) in '0'..'9' or
                      Get(Code, Index) in 'a'..'f' or
                      Get(Code, Index) in 'A'..'F' loop
                    Tok.Int := Tok.Int * 16;
                    if Get(Code, Index) in '0'..'9' then
                        Tok.Int := Tok.Int +
                                   Character'Pos(Get(Code, Index)) - 48;
                    elsif Get(Code, Index) in 'a'..'f' then
                        Tok.Int := Tok.Int +
                                   Character'Pos(Get(Code, Index)) - 87;
                    else
                        Tok.Int := Tok.Int +
                                   Character'Pos(Get(Code, Index)) - 55;
                    end if;
                    Index := Index + 1;
                end loop;
            else
                while Get(Code, Index) in '0'..'9' loop
                    Tok.Int := (Tok.Int * 10)
                             + (Character'Pos(Get(Code, Index)) - 48);
                    Index := Index + 1;
                end loop;
            end if;
        elsif Is_Identifier(Get(Code, Index)) then
            Tok.Tag := Identifier_Token;
            while Is_Identifier(Get(Code, Index)) loop
                Append(Tok.Str, Get(Code, Index));
                Index := Index + 1;
            end loop;
            declare
                Str : String := To_Upper(To_String(Tok.Str));
            begin
                if Str = "R0" or Str = "NULL" or Str = "CTRL" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 0;
                elsif Str = "R1" or Str = "TTP" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 1;
                elsif Str = "R2" or Str = "PTP" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 2;
                elsif Str = "R3" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 3;
                elsif Str = "R4" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 4;
                elsif Str = "R5" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 5;
                elsif Str = "R6" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 6;
                elsif Str = "R7" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 7;
                elsif Str = "R8" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 8;
                elsif Str = "R9" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 9;
                elsif Str = "R10" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 10;
                elsif Str = "R11" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 11;
                elsif Str = "R12" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 12;
                elsif Str = "R13" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 13;
                elsif Str = "R14" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 14;
                elsif Str = "R15" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 15;
                elsif Str = "R16" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 16;
                elsif Str = "R17" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 17;
                elsif Str = "R18" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 18;
                elsif Str = "R19" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 19;
                elsif Str = "R20" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 20;
                elsif Str = "R21" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 21;
                elsif Str = "R22" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 22;
                elsif Str = "R23" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 23;
                elsif Str = "R24" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 24;
                elsif Str = "R25" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 25;
                elsif Str = "R26" or Str = "uFP" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 26;
                elsif Str = "R27" or Str = "uSP" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 27;
                elsif Str = "R28" or Str = "uLR" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 28;
                elsif Str = "R29" or Str = "uIM" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 29;
                elsif Str = "R30" or Str = "uFL" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 30;
                elsif Str = "R31" or Str = "uPC" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 31;
                elsif Str = "R32" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 32;
                elsif Str = "R33" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 33;
                elsif Str = "R34" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 34;
                elsif Str = "R35" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 35;
                elsif Str = "R36" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 36;
                elsif Str = "R37" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 37;
                elsif Str = "R38" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 38;
                elsif Str = "R39" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 39;
                elsif Str = "R40" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 40;
                elsif Str = "R41" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 41;
                elsif Str = "R42" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 42;
                elsif Str = "R43" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 43;
                elsif Str = "R44" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 44;
                elsif Str = "R45" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 45;
                elsif Str = "R46" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 46;
                elsif Str = "R47" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 47;
                elsif Str = "R48" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 48;
                elsif Str = "R49" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 49;
                elsif Str = "R50" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 50;
                elsif Str = "R51" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 51;
                elsif Str = "R52" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 52;
                elsif Str = "R53" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 53;
                elsif Str = "R54" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 54;
                elsif Str = "R55" or Str = "DR" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 55;
                elsif Str = "R56" or Str = "B0" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 56;
                elsif Str = "R57" or Str = "B1" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 57;
                elsif Str = "R58" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 58;
                elsif Str = "R59" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 59;
                elsif Str = "R60" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 60;
                elsif Str = "R61" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 61;
                elsif Str = "R62" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 62;
                elsif Str = "R63" then
                    Tok.Tag := Register_Token;
                    Tok.Int := 63;
                elsif Str = "A0" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 1;
                elsif Str = "A1" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 2;
                elsif Str = "A2" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 3;
                elsif Str = "A3" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 4;
                elsif Str = "A4" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 5;
                elsif Str = "A5" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 6;
                elsif Str = "A6" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 7;
                elsif Str = "A7" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 8;
                elsif Str = "RT" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 9;
                elsif Str = "SC" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 10;
                elsif Str = "LN" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 11;
                elsif Str = "LN" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 25;
                elsif Str = "FP" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 26;
                elsif Str = "SP" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 27;
                elsif Str = "LR" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 28;
                elsif Str = "IM" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 29;
                elsif Str = "FL" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 30;
                elsif Str = "PC" then
                    Tok.Tag := Sv_Register_Token;
                    Tok.Int := 31;
                end if;
            end;
        elsif Get(Code, Index) = ',' then
            Tok.Tag := Comma_Token;
            Index := Index + 1;
        elsif Get(Code, Index) = ':' then
            Tok.Tag := Colon_Token;
            Index := Index + 1;
        elsif Get(Code, Index) = LF then
            Tok.Tag := Newline_Token;
            Index := Index + 1;
        else
            raise Lexer_Error with "Invalid character " &
                Character'Image(Get(Code, Index));
        end if;
        return Tok;
    end Lex;

    function Lex_All(Code : in String)
    return Token_Vector.Vector is
        Tok : Token;
        Tokens : Token_Vector.Vector;
        Index : Natural := Code'First;
    begin
        Tok.Tag := Identifier_Token;
        while Tok.Tag /= EOF_Token loop
            Tok := Lexer.Lex(Code, Index);
            Token_Vector.Append(Tokens, Tok);
        end loop;
        return Tokens;
    end Lex_All;

    procedure Skip_Whitespace(Code : in String; Index : in out Natural) is
    begin
        while Index < Code'Last and
              Character'Pos(Code(Index)) <= 32 and
              Character'Pos(Code(Index)) /= 10 loop
            Index := Index + 1;
        end loop;
    end Skip_Whitespace;

    function Get(Code : in String; Index : in Natural)
    return Character is
    begin
        if Index >= Code'Last then
            return NUL;
        end if;
        return Code(Index);
    end Get;

    function Is_Identifier(Char : in Character)
    return Boolean is
    begin
        return Char in 'a'..'z' or
               Char in 'A'..'Z' or
               Char in '0'..'9' or
               Char = '_' or Char = '@' or Char = '%' or Char = '$' or
               Char = '#' or Char = '!' or Char = '+' or Char = '-' or
               Char = '.';
    end Is_Identifier;
end Lexer;
